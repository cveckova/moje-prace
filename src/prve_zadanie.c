#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define PIS_A 65
#define PIS_B 66
#define PIS_C 67
#define PIS_D 68

typedef struct
{
	unsigned int dlzka;
	int *pole;
}MNOZINA;

void vypis(MNOZINA *M,int p)
{
	int i;
	
	printf("%c\n",p);
	
	for(i=0;i<M->dlzka;i++)
		printf("%d\t",M->pole[i]);
		
	printf("\n");
}

void nastav(MNOZINA *M)
{
	M->pole=malloc(M->dlzka*sizeof(int));
	if(M==NULL)
		return;
	
}

void destroy(MNOZINA *M)
{
	free(M->pole);
	M->pole=NULL;
}

int skontroluj(MNOZINA *M,int j)
{
	int i,m=0;
	
	for(i=0;i<j;i++)
	{
		if(M->pole[j]==M->pole[i])
		{
			m++;
			break;
		}
	}
	return m;	
}

void nacitaj(MNOZINA *M)
{
	int i,kontrola;
	
	for(i=0;i<M->dlzka;i++)
	{	
		do
		{
			M->pole[i]=rand()%30;
			kontrola=skontroluj(M,i);
		}while(kontrola!=0);
	}
}

void zjednotenie(MNOZINA *A,MNOZINA *B,MNOZINA *C)
{
	int i,j=0;
	
	for(i=0;i<A->dlzka;i++)
		C->pole[i]=A->pole[i];
	
	for(i=A->dlzka;i<C->dlzka;i++)
	{
		C->pole[i]=B->pole[j];
		j++;
	}
}

void uprav_zjednotenie (MNOZINA *C)
{
	int i,j,k=0,vyskyt=0;//pomocna
	
	//pomocna=C->dlzka;
		
	for(i=0;i<C->dlzka;i++)
	{
		vyskyt=0;
		for(j=0;j<i;j++)
		{
			if(C->pole[i] == C->pole[j])
			{
				vyskyt=1;
				//pomocna--;
			}
		}
		if(vyskyt==0) 
		{
			C->pole[k]=C->pole[i];
			k++;
		}
	}
	//C->dlzka=pomocna;
}

void prienik (MNOZINA *A,MNOZINA *B,MNOZINA *D)
{
	int i,j,m=0;
	
	for(i=0;i<A->dlzka;i++)
	{
		for(j=0;j<B->dlzka;j++)
		{
			if(A->pole[i] == B->pole[j])
			{
				D->pole[m]=B->pole[j];
				m++;
			}
		}
	}
	//D->dlzka=m;
}

int main(void)
{
	MNOZINA A,B,C,D;
	
	do
	{
		printf("Zadajte velkost mnoziny A(max 30):");
		scanf("%d",&A.dlzka);
	}while(A.dlzka>30);
	
	do
	{
		printf("Zadajte velkost mnoziny B(max 30):");
		scanf("%d",&B.dlzka);
	}while(B.dlzka>30);
		
	srand(time(0));
	
	C.dlzka=A.dlzka+B.dlzka;
	D.dlzka=A.dlzka;
	
	nastav(&A);
	nastav(&B);
	nastav(&C);
	nastav(&D);
	nacitaj(&A);
	nacitaj(&B);
	vypis(&A,PIS_A);
	vypis(&B,PIS_B);
	zjednotenie(&A,&B,&C);
	uprav_zjednotenie(&C);
	vypis(&C,PIS_C);
	prienik(&A,&B,&D);
	vypis(&D,PIS_D);
	destroy(&A);
	destroy(&B);
	destroy(&C);
	destroy(&D);
	
	return 0;
}
